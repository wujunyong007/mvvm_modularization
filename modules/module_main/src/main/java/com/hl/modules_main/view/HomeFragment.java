package com.hl.modules_main.view;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.hl.base_module.constant.ArouterPath;
import com.hl.base_module.page.BaseFragment;
import com.hl.modules_main.R;
import com.hl.modules_main.databinding.FragmentHomeBinding;
import com.hl.modules_main.view.event.HomeEventHandler;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@Route(path = ArouterPath.HOME_FRAGMENT)
public class HomeFragment extends BaseFragment<FragmentHomeBinding> {
    // 碎片databinding一把
    private FragmentHomeBinding fragmentHomeBinding;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PersonalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void initLayout(Context context) {
        fragmentHomeBinding = getViewDataBinding();
    }

    @Override
    public void requestData(Context context) {

    }

    @Override
    public void eventHandler(Context context) {
        // 注册事件对象
        if (null == fragmentHomeBinding.getClickHandler()) {
            fragmentHomeBinding.setClickHandler(new HomeEventHandler(this));
        }
    }
}
