package com.hl.base_module.appcomponent;

import com.hl.base_module.CommonApi;
import com.hl.base_module.util.SharedPreferencesUtil;

/**
 * 用户本地信息管理
 */
public class UserManager {
    public static String getName(){
        return SharedPreferencesUtil.getInstance(CommonApi.getApplication()).getSP(userKey());
    }

    public static void saveUser(String name){
        SharedPreferencesUtil.getInstance(CommonApi.getApplication()).putSP(userKey(), name);
    }

    private static String userKey(){
        return "user_key";
    }
}
